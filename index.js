const puppeteer = require('puppeteer');
const jsonfile = require('jsonfile');
const fs = require('fs');
const stringSimilarity = require('string-similarity');

const firebase = require('firebase');
const firebaseConfig = {
    apiKey: "AIzaSyAyCVgmKPapKoyDhaJ4bdQ2m4zuxhkK32Y",
    authDomain: "centralscrape.firebaseapp.com",
    databaseURL: "https://centralscrape.firebaseio.com",
    projectId: "centralscrape",
    storageBucket: "centralscrape.appspot.com",
    messagingSenderId: "55433293981"
};

const savePath = './data/scraped_new.txt';
const cookiePath = './site/cookie.json';
const dataPath = './data/archive/';
const siteUrl = "http://www.thecentralbox.net/index.php";
const googleUrl = "https://www.google.com/";

//replace here
const siteDomain = "http://www.thecentralbox.net/";
const forumuid = "160-pop-latino";

const diceCeThrs = 0.87;

const creds = {
    username: "TCB Staff3",
    pass: "123456789"
}

Array.prototype.subarray=function(start,end){
    if(!end){ 
        end = -1;
    }
    return this.slice(start, this.length+1-(end*-1));
}

String.prototype.contains = function(it) { return this.indexOf(it) != -1; };

async function getCookies(page) {
    var cookiesObject = await page.cookies();
    jsonfile.writeFile(cookiePath, cookiesObject, { spaces: 2 }, function(err) { 
        if (err) {
            console.log('The file could not be written.', err);
        }
        console.log('Session has been successfully saved');
    })
}

async function getForumData(page, browser, inset = 0, pagenum) {
    const hasCookie = fs.existsSync(cookiePath);
    if (hasCookie) {
        const cookiesArr = require(`${cookiePath}`);
        if (cookiesArr.length !== 0) {
            for (let cookie of cookiesArr) {
              await page.setCookie(cookie);
            }
            console.log('Session has been loaded in the browser');
            var forumUrl = "";
            if(pagenum == 0) {
                forumUrl = siteDomain + forumuid;
            } else {
                forumUrl = siteDomain + forumuid + "/pagina-" + pagenum;
            }
            await page.goto(forumUrl, { waitUntil: 'networkidle2' });
            const titleData = await page.evaluate(()=>{
                var data = [];
                const titles = document.querySelectorAll(".threadtitle");
                for (var i = 0; i < titles.length; i++) {
                    console.log(titles[i].innerText);
                    data.push(titles[i].innerText);
                }
                return data;
            })
            titleData.splice(0, 1); // removing table header
            console.log("Data Loaded!", titleData);
            console.log("Total entries : " + titleData.length);
            //process data
            for(data of titleData.subarray(inset, -1)) {
                let entryname = await getDataFromGoogle(browser, data);
                console.log("Done : " + entryname);
            }
        }
    } else {
        console.log('Cookie not found');
    }
}

function validateYear(year) {
    var text =  /^[0-9]+$/;
    if (year != 0) {
        var currentYear = new Date().getFullYear();
        
        if ((year != "") && (!text.test(year))) {
            // alert("Please Enter Numeric Values Only");
            return false;
        }
        if (year.length != 4) {
            // alert("Year is not proper. Please check");
            return false;
        }
        if((year < 1920) || (year > currentYear))
        {
            // alert("Year should be in range 1920 to current year");
            return false;
        }
        return true;
    }
}
// Process Data

function sanitizeTitleData(data) {
    var morphed = data;

    //replacing braces elements
    morphed = morphed.replace(/ *\[[^\]]*]/, '');
    morphed = morphed.replace(/ *\([^)]*\) */g, ""); // / *\([^)]*\) */g, ""    / *\[[^\]]*]/, ''

    //custom sanitization for other cases
    if(morphed.indexOf('>>') > -1) {
        morphed = morphed.substring(morphed.indexOf('>>') + 2);
    }
    if(morphed.indexOf('feat.') > -1) {
        morphed = morphed.substring(morphed.indexOf('feat.') + 5);
    }
    if(morphed.indexOf('feat') > -1) {
        morphed = morphed.substring(morphed.indexOf('feat') + 4);
    }
    if(morphed.indexOf('Feat.') > -1) {
        morphed = morphed.substring(morphed.indexOf('Feat.') + 5);
    }
    if(morphed.indexOf('Feat') > -1) {
        morphed = morphed.substring(morphed.indexOf('Feat') + 4);
    }
    if(morphed.indexOf('ft.') > -1) {
        morphed = morphed.substring(morphed.indexOf('ft.') + 3);
    }
    if(morphed.indexOf('ft') > -1) {
        morphed = morphed.substring(morphed.indexOf('ft') + 2);
    }
    if(morphed.indexOf('Ft.') > -1) {
        morphed = morphed.substring(morphed.indexOf('Ft.') + 3);
    }
    if(morphed.indexOf('f.') > -1) {
        morphed = morphed.substring(morphed.indexOf('f.') + 2);
    }
    if(morphed.indexOf('Live ')) {
        morphed = morphed.replace('Live ', '');
    }
    
    if(morphed.indexOf('live ')) {
        morphed = morphed.replace('live ', '');
    }

    console.log("Sanitized search string : " + morphed);
    return morphed;
}

function sanitizeTitleDataArray(data) {
    var temp = [];
    data.forEach(element => {
        temp.push(sanitizeTitleData(element)); 
    });
    return temp;
}

function writeDataToFile(dataText) {
    fs.appendFile(savePath, dataText+"\n", function (err) {
        if (err) throw err;
        console.log('Saved : ' + dataText);
    });
}

async function getDataFromGoogle(browser, data) {
    var sqmatches = data.match(/\[(.*?)\]/);
    var fmatches = data.match(/\((.*?)\)/);

    if(data.contains("Moved:")) {
        writeDataToFile(data);
        return data;
    }
    
    if(sqmatches) {
        for(match of sqmatches) {
            if(validateYear(match)) {
                writeDataToFile(data);
                return data;
            }
        }
    } 
    
    if(fmatches) {
        for(match of fmatches) {
            if(validateYear(match)) {
                var formattedData = data.substring(0, data.indexOf(match) - 1) 
                formattedData += data.substring(data.indexOf(match) + 5 , data.length) 
                formattedData += " [" + match + "]";
                writeDataToFile(formattedData);
                return data;
            }
        }
    } 

    qdata = sanitizeTitleData(data);
    const gpage = await browser.newPage();
    await gpage.setViewport({width: 1200, height: 720});
    await gpage.goto(googleUrl, { waitUntil: 'networkidle2', timeout: 0 });
    await gpage.type('input[name=q]', qdata+" release date"+String.fromCharCode(13)); //, { delay: 100 }
    await gpage.waitForSelector('div[class="Z0LcW"]'); //|| gpage.waitForSelector('div[class="Z0LcW AZCkJd"]');
    const relYear = await gpage.evaluate(() => {
        const year = document.querySelector(".Z0LcW").innerText; //|| document.querySelector(".Z0LcW .AZCkJd").innerText;
        // console.log(year);
        return year;
    });
    let formedText = data + " [" + relYear + "]";
    // console.log(formedText);
    writeDataToFile(formedText);
    await gpage.close();
    return data;
}

async function getDataWithInset(slnum, pagenum) {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.setViewport({width: 1200, height: 720});
    await getForumData(page, browser, slnum, pagenum);
}

async function login(pagenum) {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.setViewport({width: 1200, height: 720});
    await page.goto(siteUrl, { waitUntil: 'networkidle2' });
    await page.type('#navbar_username', creds.username);
    await page.type('#navbar_password_hint', creds.pass);

    await Promise.all([
        page.click('.loginbutton'),
        page.waitForNavigation({ waitUntil: 'networkidle2' }),
        getCookies(page),
    ]);

    // await getForumData(page, browser, slnum=0, pagenum);
    // await page.close();
    return 0;
}

function extractData(input, func, item, forum) {
    var remaining = '';
    input.on('data', function(data) {
      remaining += data;
      var index = remaining.indexOf('\n');
      while (index > -1) {
        var line = remaining.substring(0, index);
        remaining = remaining.substring(index + 1);
        func(line, item, forum);
        index = remaining.indexOf('\n');
      }
    });
    input.on('end', function() {
      if (remaining.length > 0) {
        func(remaining, item, forum);
      }
    });
}

function readAndUploadDataFolder(forum) {
    fs.readdir(dataPath + forum, function(err, items) {
        if(err) {
            console.log(err);
        }
        console.log(items);
        for (item of items) {
            console.log("Processing : " + item);
            var inputStream = fs.createReadStream(dataPath + forum + "/" + item);
            extractData(inputStream, hitFire, item, forum);
        }
    });
    return;
}

function hitFire(data, item, forum) {
    console.log(item + ' Line: ' + data + ' in ' + forum);
    var indexStr = item.substring(item.indexOf("_") + 1, item.indexOf("."));
    // console.log(indexStr);
    var yearMatches = data.match(/\[(.*?)\]/);
    var yearStr = data.substring(data.indexOf("[") + 1, data.indexOf("]"));
    if(yearMatches) {
        for(matchedItem of yearMatches) {
            if(validateYear(matchedItem)) {
                yearStr = matchedItem;
            }
        }
    }
    // console.log(yearStr);
    let payload = {
        title: data.trim(),
        year: yearStr
    }
    firebase.database().ref("Scraped/" + forum + "/" + indexStr).push().set(payload, (error) => {
        if(error) {
            console.log("----------------FIREBASE ERROR-----------------")
            console.log(error);
        } else {
            console.log('Firebase UP: ' + data + " : " + item);
        }
    });
}

function removeUploaded() {
    firebase.database().ref("Scraped/" + forum).remove();
}

function updateCookies() {
    const hasCookie = fs.existsSync(cookiePath);
    if(hasCookie) {
        const cookies = require(`${cookiePath}`);
        firebase.database().ref("SiteCookies/").set(cookies).then(() => {
            console.log("Updated Cookies!");
        });
    }
}

function uploadScrapped(forum) {
    firebase.initializeApp(firebaseConfig);
    removeUploaded(forum);
    readAndUploadDataFolder(forum);
    // updateCookies();
}

const distinctVals = (value, index, self) => {
    return self.indexOf(value) === index;
}

function percentify(num) {
    return (num * 100).toFixed(2);
}

const findSimilarIn = async (pageCount, sanitize=false) => {
    var scrapedTitles = [];
    var pageHash = [];
    console.log("Starting Scraper for " + siteDomain + forumuid + "...");
    const browser = await puppeteer.launch({headless: true});
    
    for (var i = 1; i<=pageCount; i++) {
        var siteLink = "";
        if(i > 1) {
            siteLink = siteDomain + forumuid + "/pagina-" + i;
        } else {
            siteLink = siteDomain + forumuid;
        }
        const page = await browser.newPage();
        await page.setViewport({width: 1200, height: 720});
        await page.goto(siteLink, { waitUntil: 'networkidle2' });
        const titleData = await page.evaluate(()=>{
            var data = [];
            const titles = document.querySelectorAll(".threadtitle");
            for (var i = 0; i < titles.length; i++) {
                // console.log(titles[i].innerText);
                data.push(titles[i].innerText.trim());
            }
            return data;
        })
        titleData.splice(0, 1); // removing table header
        // console.log("Data Loaded!", titleData);
        console.log("HIT " + i + ": Fetched titles from " + siteLink, "Total entries : " + titleData.length);
        // console.log();

        for (title of titleData) {
            scrapedTitles.push(title.trim());
            pageHash[title.trim()] = i;
        }
        await page.close();
    }
    await browser.close();
    console.log("Scraped : "+scrapedTitles.length);
    // console.log(pageHash);
    return [scrapedTitles, pageHash];
}

function matchFinder(titles, pages) {
    
    console.log("Staring matchfinder : Using Dice coefficient Algorithm")
    const distinctTitles = titles.filter(distinctVals);
    var resultData = [];
    for (item of distinctTitles) {
        var matches = stringSimilarity.findBestMatch(item, titles);
        var similarTitles = [];
        var similarRatings = [];
        for (var i in matches.ratings) {
            if (matches.ratings[i].rating > diceCeThrs) {
                similarTitles.push(matches.ratings[i].target);
                similarRatings.push(matches.ratings[i].rating);
            }
        }
        if (similarTitles.length > 1) {
            console.log("........................................................");
            console.log(similarTitles.length + " matches Found for '"+item+"'");
            var titleMatchData = {
                title: item
            }
            var matchData = [];
            for (const [index, data] of similarTitles.entries()) {
                console.log("Match "+ index + " : " + data, 
                "| Match : " + percentify(similarRatings[index]) +"%",
                "| Page Number : " + pages[data]);
                matchData.push({
                    title: data,
                    rating: percentify(similarRatings[index]),
                    page: pages[data]
                });
            }
            titleMatchData.matches = matchData;
            resultData.push(titleMatchData);
        }
    }

    var payload = {
        total: titles.length,
        distincts: distinctTitles,
        matchResult: resultData 
    };
    
    console.log("\nUploading to firebase...");
    pushMatchesToFirebase(payload);
}

function pushMatchesToFirebase(payload) {
    firebase.initializeApp(firebaseConfig);
    firebase.database().ref("Matches/" + forumuid).set(payload).then(() => {
        console.log("Matches Uploaded To Firebase!");
        console.log("Press Ctrl + C to exit application");
    });
}

function getSimilarTitles(pages) {
    findSimilarIn(pages).then((scraped) => {
        console.log("Finished Scraping.");
        var titles = scraped[0];
        var pages = scraped[1];
        console.log("Total Count : "+titles.length);
        matchFinder(titles, pages);
    });
}

//Run selectively
// login(pagenum=4);
// getDataWithInset(slnum = 19, pagenum = 37);
// uploadScrapped(forum='Videos Pop');
getSimilarTitles(pages=47);